﻿namespace Core.Models
{
    public enum Days
    {
        SATURDAY = 1,
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY
    }
}