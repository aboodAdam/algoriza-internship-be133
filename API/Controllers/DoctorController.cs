﻿using Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoriza_Internship_BE133.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IDoctorService _doctorService;
        public DoctorController(IDoctorService doctorService, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _doctorService = doctorService;
        }

        [HttpPost("Login")]
        public async Task<bool> Login(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                await _signInManager.SignInAsync(user, isPersistent: false);
                return true;
            }

            return false;
        }
        
        [HttpPost("AddAppointment")]
        public bool AddAppointment([FromBody] AppointmentPayload payload)
        {
            return _doctorService.AddAppointment(payload);
        }

        [HttpPut("UpdateAppointment")]
        public bool UpdateAppointment([FromBody] AppointmentPayload payload)
        {
            return _doctorService.UpdateAppointment(payload);
        }

        [HttpDelete("DeleteAppointment")]
        public bool DeleteAppointment(int doctorId)
        {
            return _doctorService.DeleteAppointment(doctorId);
        }
    }
}
